<?php

/**
 *  @template       FE-Salesform
 *  @version        see info.php of this template
 *  @author         erpe
 *	@copyright      2010-2018 erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) )
{
    include( LEPTON_PATH . '/framework/class.secure.php' );
} 
else
{
    $oneback = "../";
    $root    = $oneback;
    $level   = 1;
    while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) )
    {
        $root .= $oneback;
        $level += 1;
    } 
    if ( file_exists( $root . '/framework/class.secure.php' ) )
    {
        include( $root . '/framework/class.secure.php' );
    } 
    else
    {
        trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR );
    }
}
// end include class.secure.php



// OBLIGATORY WEBSITE BAKER VARIABLES
$template_directory		= 'fe_salesform';
$template_name			= 'FE-Salesform';
$template_function		= 'template';
$template_delete  		=  true;
$template_version		= '1.0.0';
$template_platform		= 'Lepton 1.x';
$template_author		= 'erpe';
$template_license		= '<a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>';
$template_license_terms	= '-';
$template_description	= 'This template is for use on page where you do not want anything wrapping the content.';
$template_guid          = 'cb0c7e7f-6394-46ff-aca8-d7fd9ac10d5c';

// OPTIONAL VARIABLES FOR ADDITIONAL MENUES AND BLOCKS
$menu[ 1 ]  = 'Main';
$menu[ 2 ]  = 'Foot';
$menu[ 3 ]  = 'Top';
// $block[1]							= '';
// $block[2]							= '';

?>